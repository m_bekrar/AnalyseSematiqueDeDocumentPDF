# -*- coding: utf-8 -*-
import os
import re
from fnmatch import fnmatch
from pyPdf import PdfFileReader
from cStringIO import StringIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
import os.path
import json

############## Extraction des pages par PDFMiner################
def convert(fname, pages=None):
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)

    output = StringIO()
    manager = PDFResourceManager()
    converter = TextConverter(manager, output, laparams=LAParams())
    interpreter = PDFPageInterpreter(manager, converter)

    infile = file(fname, 'rb')
    for page in PDFPage.get_pages(infile, pagenums):
        interpreter.process_page(page)
    infile.close()
    converter.close()
    text = output.getvalue()
    output.close
    return text


# Rendre paraméttrable
root = '/Users/marwa/Desktop/oseo'
Echec = 0
NombrePDF = 0
out_file = open("LesTableaux.json", "w")
Command = 'sudo java PrintTextLocations  '
os.system("javac PrintTextLocations.java")

for path, subdirs, files in os.walk(root):
    print 'PATH', path
    for name in files:
        FileName = os.path.join(path, name)

        print 'filename', FileName
        if FileName.endswith('pdf'):

            try:
                NombrePDF += 1
                Found = False

                j = 1
                while (j < 6) and not Found:
                    txt = convert(FileName, pages=[j, j])
                    txt = txt.decode('utf-8')
                    lignes = txt.split("\n")
                    matchObj = re.search(r"(Table des Mati.*|TABLE DES MATI.*|Sommaire|SOMMAIRE|Table des mati.*)", txt)
                    Titres = []
                    if matchObj:
                        print " ******* MOT INDICATEUR DU DEBUT DE LA TOC EST DETECTE SUR LA PAGE :", j
                        pageDebut = j
                        Sommaire = matchObj.group()
                        Found = True
                        EntreesTable = []
                        #EntreesTable.append(lignes)
                        Arret = False
                        for ligne in lignes:
                            matchEntry = re.search(r'(.+)[a-zA-Z]+(\s*)([.]*\s|[-]*\s|[.]+\s*)[0-9]{1,2}\s*$', ligne,
                                                   re.M | re.I)
                            Figure = re.search(r'^(FIGURE )[1-9]+', ligne, re.M | re.I)
                            if Figure:
                                Arret = True
                            if matchEntry and not Arret:
                                i += 1
                                EntreesTable.append(matchEntry.group())
                        probleme = False
                        if len(EntreesTable) < 2:

                            ########### Le debut de la table a été bien detecté ,mais les lignes sont mal extraits;
                            #  alors on prend la page entière et on essaie de reconstruire les lignes de la TOC avec PDFBox plutard
                            print '******** Echec de reconnaissance des entrees de la table.... prise de la page entiere du sommaire  *********'
                            EntreesTable = []
                            probleme = True
                            print 'IMPRESSION LA PAGE ENIERE ', lignes
                            for ligne in lignes:
                                EntreesTable.append(ligne)
                            #############  amélioration de la table ##########

                            Clean = []

                            print ' ************ Essai d améliorer la table *****************'
                            for entree in EntreesTable:

                                entree = entree.replace('\\t', '')
                                entree = entree.replace('\\r', '')
                                entree = entree.replace('\r', '')
                                entree = entree.replace('\t', '')
                                matchObj = re.search(
                                    r"(Table des Mati.*|TABLE DES MATI.*|Sommaire|SOMMAIRE|Table des mati.*)", entree)

                                if not matchObj and not (entree == '') and not (entree == ' ') and not (
                                            entree == '   ') and not (entree == '  ') and not (entree == '  ') and not (entree == '.'):
                                    Clean.append(entree)

                            EntreesTable = []
                            EntreesTable = Clean
                            print Clean

                            if os.path.exists('temporary.txt'):
                                os.remove('temporary.txt')
                                print 'Supprimé!!!!!!!!!!!!'

                            words = []
                            for titre in EntreesTable:
                                #titre= titre.decode('utf-8')
                                mots = titre.split(' ')
                                #print mots

                                for chaine in mots:
                                    if not chaine == ' ' and not chaine == ''and not chaine == '.':
                                        words.append(chaine)

                            list = ''
                            for word in words:
                                #word = str(word)
                                word = word.replace('(', '')
                                word = word.replace(')', '')

                                list = list + ',' + word

                            print 'list :', list
                            page = str(pageDebut)
                            Coordonnes = Command + ' ' + FileName + ' ' + list + ' ' + page
                            #print 'Coordonnes : ', Coordonnes
                            try:
                                os.system(Coordonnes.encode('utf-8'))
                            except e, Exception:
                                print e
                                pass


                            if os.path.exists('temporary.txt'):
                                f = open('temporary.txt')
                                content = f.readlines()
                                f.close()
                                max = len(content)
                                #print '\n'
                                liste = str(content[max - 1])
                                #print type(liste)
                                #print '\n'
                                print liste
                                listee = liste.split(',')
                                #print '\n'
                                #print listee
                                X = []
                                Y = []
                                Word = []

                                for list in listee:
                                    #print list
                                    list = list.replace('[', '')
                                    list = list.replace(']', '')
                                    list = list.replace('(1)', '')
                                    list = list.split(' ')
                                    temporaire = []
                                    for l in list:
                                        if not l == '' and not l == ':':
                                            temporaire.append(l)

                                    X.append(temporaire[0])
                                    Y.append(temporaire[1])
                                    Word.append(temporaire[2])

                                #for i in range (0, len(Y)-1):
                                    #print Y[i],':', Word[i]

                                ulist = []
                                [ulist.append(x) for x in Y if x not in ulist]
                                #print '********* liste unique des Y:', ulist

                                for y in ulist:
                                    appar = []
                                    for i in range (0, len(Y)-1):

                                        if y == Y[i]:
                                            appar.append(i)

                                    titre = ''
                                    for i in appar:
                                        titre = titre+ ' ' + Word[i]

                                    if not titre == '':
                                        Titres.append(titre)

                                print 'La table des matieres :', Titres
                                h = 0
                                for titre  in Titres:
                                    fausseTable = re.search('[0-9]+', titre)
                                    if fausseTable:
                                        h+=1
                                if len(Titres) == h:
                                    #print 'Fausse Table !!!!!!!!'
                                    Titres = Clean
                            else:
                                Titres = Clean


                    j += 1

                Detect = False
                if Found == False and not Detect:
                    print '******* mot cle sommaire non trouvé, recherche de la table en cours ......'
                    j = 1
                    while (j < 6) and not Detect:
                        txt = convert(FileName, pages=[j, j])
                        txt = txt.decode('utf-8')
                        lignes = txt.split("\n")
                        i = 0
                        EntreesTable = []
                        Arret = False
                        for ligne in lignes:
                            matchEntry = re.search(r'(.+)[a-zA-Z]+(\s*)([.]*\s|[-]*\s|[.]+\s*)[0-9]{1,2}\s*$', ligne,
                                                   re.M | re.I)
                            Figure = re.search(r'^(FIGURE )[1-9]+', ligne, re.M | re.I)
                            if Figure:
                                Arret = True
                            if matchEntry:
                                i += 1
                                EntreesTable.append(matchEntry.group())
                                #print matchEntry.group()

                        if i > 30:
                            #print "La page de la table des matieres est : ", j
                            pageDebut = j
                            Detect = True

                        j += 1

                    if Detect == False:
                        print 'Echec de la detection de la page de la table des matieres'

                Deux = False

                if Found or Detect:
                    print 'verification si la table est sur deux pages .....'
                    k = pageDebut + 1
                    txt = convert(FileName, pages=[k, k])
                    txt = txt.decode('utf-8')
                    lignes = txt.split("\n")
                    NbreEntry = 0
                    Arret = False
                    DeuxiemeTable = []
                    for ligne in lignes:
                        EntrySecondPage = re.search(r'(.+)[a-zA-Z]+(\s*)([.]*\s|[-]*\s|[.]+\s*)[0-9]{1,2}\s*$', ligne,
                                                    re.M | re.I)
                        Figure = re.search(r'^(FIGURE )[1-9]+', ligne, re.M | re.I)
                        if Figure:
                            Arret = True
                        if EntrySecondPage and not Arret:
                            NbreEntry += 1
                            DeuxiemeTable.append(EntrySecondPage.group())

                    if NbreEntry >= 4:
                        print 'xxxxxxxxxxxxxxxx la table est sur deux pages xxxxxxxxxxxxxxx'
                        DeuxiemePage = k
                        Deux = True
                    else:
                        print 'pas sur deux pages'

            except Exception, e:
                print 'Exception 1 : ', e


            ########### Extraire la table ##########
            try:
                counted = False
                if Found:
                    if probleme:
                        Titress = []
                        #Titress = Titres
                        for titre in Titres:
                            pag = re.search(r'\s[0-9]{1,2}\s*$', titre)
                            if pag:
                                num = pag.group()
                                chaine = titre.replace(num, '')
                                Titress.append(chaine)
                                Titress.append(num)
                            else:
                                Titress.append(titre)




                    else:
                        Titress = []
                        for titre in EntreesTable:
                            pag = re.search(r'\s[0-9]{1,2}\s*$', titre)
                            if pag:
                                num = pag.group()
                                chaine = titre.replace(num, '')
                                Titress.append(chaine)
                                Titress.append(num)
                            else:
                                Titress.append(titre)

                    if Deux:
                       for entree in DeuxiemeTable:
                            pag = re.search(r'\s[0-9]{1,2}\s*$', entree)
                            if pag:
                                num = pag.group()
                                chaine = titre.replace(num, '')
                                point = re.search(r'[.]{2,1000}', chaine)
                                if point:
                                    points = point.group()
                                    chaine = chaine.replace(points, '')

                                Titress.append(chaine)
                                Titress.append(num)
                            else:
                                Titress.append(titre)

                if Detect:
                    if not Deux:
                        Titress = []
                        for titre in EntreesTable :
                            pag = re.search(r'\s[0-9]{1,2}\s*$', titre)
                            if pag:
                                num = pag.group()
                                chaine = titre.replace(num, '')
                                point = re.search(r'[.]{2,1000}', chaine)
                                if point:
                                    points = point.group()
                                    chaine = chaine.replace(points, '')
                                Titress.append(chaine)
                                Titress.append(num)
                            else:
                                Titress.append(titre)

                    if Deux:
                        print 'Premiere page du sommaire', pageDebut
                        print EntreesTable
                        Titress = []
                        for titre in EntreesTable :
                            pag = re.search(r'\s[0-9]{1,2}\s*$', titre)
                            if pag:
                                num = pag.group()
                                chaine = titre.replace(num, '')
                                point = re.search(r'[.]{2,1000}', chaine)
                                if point:
                                    points = point.group()
                                    chaine = chaine.replace(points, '')
                                Titress.append(chaine)
                                Titress.append(num)
                            else:
                                Titress.append(titre)
                        print "Deuxieme page  du sommaire", DeuxiemePage
                        print DeuxiemeTable
                        for titre in DeuxiemePage :
                            pag = re.search(r'\s[0-9]{1,2}\s*$', titre)
                            if pag:
                                num = pag.group()
                                chaine = titre.replace(num, '')
                                point = re.search(r'[.]{2,1000}', chaine)
                                if point:
                                    points = point.group()
                                    chaine = chaine.replace(points, '')
                                Titress.append(chaine)
                                Titress.append(num)
                            else:
                                Titress.append(titre)
                if not Detect and not Found:
                    Echec+=1
                    counted = True
                    Titress = []
                    Titress.append('Echec')
                if (len(EntreesTable) < 2) and not counted:
                    Echec+=1
                Exemple = {}
                Exemple['Document'] = FileName
                Exemple['Table des matieres'] = Titress
                json.dump(Exemple, out_file, indent=4)
            except Exception, e:
              print 'Exception 2 : ',e


print "Nombre d'echec d'extraction :", Echec, "sur ", NombrePDF
out_file.close()


