# -*- coding: utf-8 -*-
import elasticsearch
import random
import io
import re
import json
from cStringIO import StringIO
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from pyPdf import PdfFileReader
import gensim
import urllib2
import datetime
import requests
from urllib import urlopen
from lxml import etree
import elasticsearch
import random

#######
def extract_urls(your_text):
    url_re = re.compile(
        r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?\xab\xbb\u201c\u201d\u2018\u2019]))')
    for match in url_re.finditer(your_text):
        yield match.group(0)


############## Extraction des pages par PDFMiner################
def convert(fname, pages=None):
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)

    output = StringIO()
    manager = PDFResourceManager()
    converter = TextConverter(manager, output, laparams=LAParams())
    interpreter = PDFPageInterpreter(manager, converter)

    infile = file(fname, 'rb')
    for page in PDFPage.get_pages(infile, pagenums):
        interpreter.process_page(page)
    infile.close()
    converter.close()
    text = output.getvalue()
    output.close
    return text


################## Remplacement des chaines de caractères#########################
def lreplace(pattern, sub, string):
    """
    remplace au début
    """
    return re.sub('^%s' % pattern, sub, string)


def rreplace(pattern, sub, string):
    """
    remplace à la fin
    """
    return re.sub('%s$' % pattern, sub, string)


########
def whatisthis(s):
    if isinstance(s, str):
        print "ordinary string"
    elif isinstance(s, unicode):
        print "unicode string"
    else:
        print "not a string"

############################ stop words
StopWords = []
f = open("SmartStoplist.txt", 'r')
content = f.readlines()

for line in content:
    matchToken = re.search('(.*)', line)
    if matchToken:
        stop = matchToken.group().decode("UTF-8")
        StopWords.append(stop)



StopWWords = ''
f = open("SmartStoplist.txt", 'r')
content = f.readlines()
f.close()
for line in content:
    matchToken = re.search('(.*)', line)
    if matchToken:
        stop  = matchToken.group().decode("UTF-8")
        stop = stop.replace('\r', '')
        StopWWords = StopWWords+' '+stop

################### Récupération du texte extrait par elasticSearch #####################

indexe = 'test'
type = 'attachment'
now = datetime.datetime.now().date()
now = str(now)
from nltk.tag.stanford import NERTagger
st = NERTagger('/usr/share/stanford-ner-2015-01-30/classifiers/english.all.3class.distsim.crf.ser.gz','/usr/share/stanford-ner-2015-01-30/stanford-ner-3.5.1.jar')
from nltk.tag.stanford import POSTagger
french_postagger = POSTagger('models/french.tagger', 'stanford-postagger.jar')
model = gensim.models.Word2Vec.load("wiki.fr.word2vec.model")
doc = []
es = elasticsearch.Elasticsearch(["127.0.0.1:9200"])

res = es.search(index=indexe, body=
{
    "fields": [
        "file"
    ]
}, size=1000)
random.seed(1)
documentName = []
sample = res['hits']['hits']

out_file = open("KeyWords.json", "w")

print("Got %d Hits:" % res['hits']['total'])
l=1
for hit in sample:

    if (l ==1):
        try:
            metaKeys=[]
            print '+++++++++++++ id +++++++++++\n'
            col1 = hit["_id"]
            print 'id= ' + col1

            reslien = es.get(index = indexe, doc_type=type, id=col1)
            col3 = reslien['_source']['title']
            col3 = str(col3)
            print '++++++++++++ titre ++++++++++++\n'
            print 'titre= ' + col3
        except Exception, e:
            print '++++++++++++ titre ++++++++++++\n'
            col3 = ""
            print 'titre=' + col3
        try:
            print '+++++++++++++++++++ liens ++++++++++++++++++++\n'
            col2 = hit["fields"]["file"][0]
            filename = "newfile.txt"

            ############## Ecriture et lecture du text extrait d'ElasticSearch dans le fichier ####################
            f = io.open(filename, 'w', encoding='utf8')
            f.write(col2)
            f.close()
            f = io.open(filename, 'r', encoding='utf8')
            content = f.readlines()
            max = len(content)
            print "taille du fichier:", max
            f.close()
            ##################### Detection de la premiere ligne de la table des matieres ####################
            Debut = 0
            i = 0
            trouves = False
            maxligne = int (max/4)
            while (i <= maxligne) and (not trouves):
                matchObj = re.search(r"(Table des Mati.*|TABLE DES MATI.*|Sommaire|SOMMAIRE|Table des mati.*)", content[i])
                if matchObj:
                    print matchObj.group()
                    Debut = i
                    print "Debut de la table", Debut
                    start = Debut + 1
                    trouves = True
                i += 1


            ############################### Detection des lignes de la table des matieres et leurs ecritures dans une table#####################
            i = start
            Titre = []
            Table = []
            delete = False
            taille = -1
            tailleTitre = -1
            k = 0
            maxline = start + 80
            # print "i= ", i
            while (i <= maxline) and (not delete):
                matchEntry = re.search(
                    r'(.+)[a-zA-Z]+(\s*)([.]*\s|[-]*\s|[.]+\s*)[0-9]{1,2}\s*$',
                    content[i], re.M | re.I)
                if matchEntry:
                    delete = re.search(r'^(FIGURE|TABLE)+\s+[0-9]', content[i], re.M | re.I)
                    if not delete:
                        k += 1
                        try:
                            num = re.search(
                                r'([0-9]+\)*\s+|[0-9]+\.[0-9]*\)*\.*\s+|[a-zA-Z]\.\s+|[a-zA-Z]\)\s+|[a-zA-Z]\.[a-zA-Z]\)*\.\s+|[IXVLCDM]+\.\s+|[IXVLCDM]+\)\s+|[IXVLCDM]+\.[0-9]*\.*[a-zA-Z]*\.*\)*\s+|[a-zA-Z]\.[0-9]\)*\s+)',
                                content[i], re.M | re.I)
                            if num:
                                numero = num.group()
                            else:
                                numero = " "
                        except e, Exception:
                            numero = " "
                            pass

                        Table.append(numero)
                        chaine = content[i]
                        chaine.encode('utf-8')
                        chaine = lreplace(numero, "", chaine)
                        pag = re.search(r'\s[0-9]{1,2}\s', content[i])
                        if pag:
                            page = pag.group()
                            chaine = rreplace(page, "", chaine)
                            numpage = re.search(r'[0-9]+', page, re.M | re.I)
                            if numpage:
                                page = numpage.group()

                            chaine = chaine.replace('\\n', '')
                            page = "page:" + page
                        else:
                            page = ""
                        chaine = chaine.replace('.', "")
                        Table.append(chaine)
                        Table.append(page)
                        ################ Table des titres
                        print matchEntry.group()
                        titre = numero+chaine
                        Titre.append(titre)
                        Titre.append(page)
                        #print titre
                        taille += 3
                        tailleTitre += 2
                    if delete:
                        fin = i
                        # deleted = True
                        print "Fin du Tableau", fin
                i += 1

            print 'Reconnaissance et Extraction de la table sont finis'
            print "nombre de ligne dans la table des matieres: ", k
            print Table
            print Titre

            #######################Extraction avec PDFMINER si ça existe dans le document#########
            count = 0
            sauvgarde = 0
            existe = False
            pdf = PdfFileReader(open(col3, 'rb'))
            pages = pdf.getNumPages()
            pagess = int(pages/5)
            print pagess
            for i in range(0, pagess):
                txt = convert(col3, pages=[i, i])
                txt = txt.decode('utf-8')
                match = re.search(r'(\s*R\xe9sum\xe9\s*|\s*Resume\s*|RESUME OPERATIONNEL|contexte du projet|R\xe9sum\xe9 ex\xe9cutif|RESUME EXECUTIF|R\xe9sum\xe9 op\xe9rationel)', txt, re.M | re.I)
                if match:
                    count += 1
                    matchObj = re.search(r"(Table des mati.*|Sommaire)", txt, re.M | re.I)
                    if matchObj:
                        count -= 1
                        print "Elle existe dans la table des matieres dans la page", i
                    else:
                        sauvgarde = i
                        print "la vrai page :", i
                        existe = True
                        print txt
                        try:
                            ChoixFinal = txt
                        except e, Exception:
                            ChoixFinal = "   "
                            pass
            print 'count:', count

            if (count == 0) and (not existe):
                for i in range(0, pagess):
                    txt = convert(col3, pages=[i, i])
                    txt = txt.decode('utf-8')
                    match = re.search(r'(\s*R\xe9sum\xe9\s*|\s*Resume\s*|RESUME OPERATIONNEL|contexte du projet|R\xe9sum\xe9 ex\xe9cutif|RESUME EXECUTIF|R\xe9sum\xe9 op\xe9rationel)', txt, re.M | re.I)
                    if match:
                        count += 1
                        matchObj = re.search(r"(Table des mati.*|Sommaire)", txt, re.M | re.I)
                        if matchObj:
                            count -= 1
                            print "Elle existe dans la table des matieres dans la page", i
                        else:
                            sauvgarde = i
                            print "la vrai page :", i
                            print txt
                            try:
                                ChoixFinal = txt
                            except e, Exception:
                                ChoixFinal= '      '
                                pass
                            #print "type résumé :", type(ChoixFinal)



            Abstract = ChoixFinal.encode("UTF-8")
            Abstract = str(Abstract)
            Exemple = {}
            Exemple['_id'] = col1
            Exemple['Titre'] = col3
            try:
                Exemple['Abstract_Français'] = ChoixFinal
            except e, Exception:
                Exemple['Abstract_Français'] = '   '
                pass
            try:

                lastTry = re.search(ur"(’|‘)", Abstract, re.UNICODE)
                if lastTry:
                    Abstract = Abstract.replace(lastTry.group(),' ')
                matchCode = re.search(r"[']", Abstract)
                if matchCode:
                    Abstract = Abstract.replace(matchCode.group(),' ')

                text=Abstract
                print text
                payload = {'q': "select * from contentanalysis.analyze where text='{text}'".format(text=text)}
                r = requests.post("http://query.yahooapis.com/v1/public/yql", data=payload)
                print(r.text)
                #faire sortir comme fonction
                xmll = r.text
                xmll = xmll.encode("UTF-8")
                f =  open("myxmlfile.xml", "wb")
                f.write(xmll)
                f.close()


                f = io.open("myxmlfile.xml", 'r', encoding='utf8')
                Keyss = []
                #score = []
                from xml.dom import minidom
                xmldoc = minidom.parse("myxmlfile.xml")
                itemlist = xmldoc.getElementsByTagName('yctCategory')
                print ("Len : ", len(itemlist))
                for s in itemlist :
                    #print ("Score : ", s.attributes['score'].value)
                    #score.append(s.attributes['score'].value)
                    print ("category : ", s.firstChild.nodeValue)
                    Keyss.append(s.firstChild.nodeValue)

                print ("la liste d'XML",Keyss)
                Exemple ["Yahoo_Français"] = Keyss

               ################Englais
                text=Abstract
                import azure_translate_api

                client = azure_translate_api.MicrosoftTranslatorClient('270590',  # make sure to replace client_id with your client id
                                                       'v2p+hDp6kHz8c5EdW5leU5IlIudc802wvaMvDc3AH0Y=') # replace the client secret with the client secret for you app.
                text = client.TranslateText(text, 'fr', 'en')
                print text
                Exemple["Abstract_English"] = text
                text = str(text)
                lastTry = True
                while lastTry:
                    lastTry = re.search(r'\u000a', text)

                    if lastTry:
                        text=text.replace(lastTry.group(), '')

                lastTry = True
                while lastTry:
                    lastTry = re.search(r'\u000b', text)

                    if lastTry:
                        text=text.replace(lastTry.group(), '')
                lastTry = True
                while lastTry:
                    lastTry = re.search(r'\u000c', text)

                    if lastTry:
                        text=text.replace(lastTry.group(), '')

                lastTry = True
                while lastTry:
                    lastTry = re.search(r'\u000d', text)

                    if lastTry:
                        text=text.replace(lastTry.group(), '')


                lastTry = True
                while lastTry:
                    lastTry = re.search(r"[']", text)

                    if lastTry:
                        text=text.replace(lastTry.group(), '')

                lastTry = True
                while lastTry:
                    lastTry = re.search(r"/", text)
                    if lastTry:
                        text=text.replace(lastTry.group(), '')

                print text

                payload = {'q': "select * from contentanalysis.analyze where text='{text}'".format(text=text)}
                r = requests.post("http://query.yahooapis.com/v1/public/yql", data=payload)
                print(r.text)
            #faire sortir comme fonction
                xmll = r.text
                xmll = xmll.encode("UTF-8")
                f =  open("myxmlfile.xml", "wb")
                f.write(xmll)
                f.close()
                #Faire sortir comme fonction

                f = io.open("myxmlfile.xml", 'r', encoding='utf8')
                Keys = []
                score = []
                from xml.dom import minidom
                xmldoc = minidom.parse("myxmlfile.xml")
                itemlist = xmldoc.getElementsByTagName('yctCategory')
                print ("Len : ", len(itemlist))
                for s in itemlist :
                    print ("Score : ", s.attributes['score'].value)
                    score.append(s.attributes['score'].value)
                    print ("category : ", s.firstChild.nodeValue)
                    Keys.append(s.firstChild.nodeValue)

                print ("la liste d'XML",Keys)


                itemlist = xmldoc.getElementsByTagName('text')
                print ("Len : ", len(itemlist))
                for s in itemlist :
                    #print ("Score : ", s.attributes['score'].value)
                    #score.append(s.attributes['score'].value)
                    print ("entity : ", s.firstChild.nodeValue)
                    Keys.append(s.firstChild.nodeValue)


                print Keys
                Exemple["Yahoo_English"]= Keys
                from elasticsearch import Elasticsearch
                ess = Elasticsearch()

                ress = ess.termvector(index='test', doc_type='attachment', id=hit["_id"],field_statistics=False, fields='file', offsets=False, positions=False) #AUy9rugKHq7548eTI7tF

                liste = ress['term_vectors']['file']['terms']
                mots = []
                frequence = []

                for token in liste:
                    mots.append(token)
                    frequence.append(liste[token]['term_freq'] )

                Stop = []
                for stop in StopWords:

                    stop = stop.replace('\r', '')
                    Stop.append(stop)

                z = len(mots)

                newListe = []

                for mot in mots:
                    if not(mot in Stop):
                        newListe.append(mot)
                z=len(newListe)

                lastListe = []
                for mot in newListe:
                    matchNumber = re.search(r'^[0-9]+',mot)
                    if not matchNumber:
                        lastListe.append(mot)

                mot_freq =[]
                for key in lastListe:
                    mot_freq.append(key)
                    mot_freq.append(liste[key]['term_freq'])


                j=0
                Table =[]
                Sable =[]
                while j<len(mot_freq) :

                    matchChar=re.search(r"^[a-zA-Z][']", mot_freq[j])
                    if matchChar:

                        mot = mot_freq[j].replace(matchChar.group(),'')
                    else:
                        matchCode=re.search(r"[']", mot_freq[j])
                        if matchCode:
                            mot = mot_freq[j].replace(matchCode.group(),' ')
                        else:
                            regex = re.search(ur"^([a-zA-Z]’|[a-zA-Z]‘)", mot_freq[j], re.UNICODE)
                            if regex :
                                mot = mot_freq[j].replace(regex.group(),'')
                            else:
                                lastTry = re.search(ur"(’|‘)", mot_freq[j], re.UNICODE)
                                if lastTry:
                                    mot = mot_freq[j].replace(lastTry.group(),' ')
                                else:
                                    mot = mot_freq[j]
                    Table.append(mot)
                    Sable.append(mot_freq[j+1])

                    j += 2
                ####### Enlever de nouveau les stopwords après l'enlèvement des apostrophes

                laPlusPropre =[]
                laPlusPropreAvecFreq=[]
                for i in range(0,len(Table)):
                    if not (Table[i] in Stop):
                        laPlusPropre.append(Table[i])
                        laPlusPropreAvecFreq.append(Table[i])
                        laPlusPropreAvecFreq.append(Sable[i])

                ######## LSA, TF-IDF m prepare le document
                j=0
                document = ''
                while (j < len(laPlusPropreAvecFreq)):
                    for i in range (0, laPlusPropreAvecFreq[j+1]):
                        document = document+' '+ laPlusPropreAvecFreq[j]
                    j+=2
                doc.append(document)
                documentName.append(col3)
                from gensim import corpora, models, similarities

                documents = []



                # remove common words and tokenize
                documents.append(document)
                documents.append('pour avoir la ')

                f = open('mycorpus.txt','w')
                document = document.encode("UTF-8")
                f.write(document)
                f.close()

                stoplist = set(StopWWords.split())

                texts = [[word for word in document.lower().split() if word not in stoplist]
                          for document in documents]
                #print texts

                # remove words that appear only once
                from collections import defaultdict
                frequency = defaultdict(int)
                for text in texts:
                    for token in text:
                        frequency[token] += 1
                texts = [[token for token in text if frequency[token] > 1]
                          for text in texts]
                #from pprint import pprint   # pretty-printer
                #pprint(texts)

                dictionary = corpora.Dictionary(texts)
                dictionary.save('/tmp/deerwester.dict') # store the dictionary, for future reference
                #print(dictionary)
                #print(dictionary.token2id)

                corpus = [dictionary.doc2bow(text) for text in texts]
                #
                #print corpus

                corpora.MmCorpus.serialize('/tmp/deerwester.mm', corpus)
                import gensim
                from gensim import corpora, models, similarities
                from gensim import *
                dictionary = corpora.Dictionary.load('/tmp/deerwester.dict')
                corpus = corpora.MmCorpus('/tmp/deerwester.mm')

                tfidf = models.TfidfModel(corpus)

                print 'TFIDF    :', tfidf

                corpus_tfidf = tfidf[corpus]
                #firstTime = True
                #for docc in corpus_tfidf:
                    #if firstTime:
                        #Exemple['Latent Semantic Indexing'] = docc
                        #firstTime = False

                lsi = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=2) # initialize an LSI transformation
                corpus_lsi = lsi[corpus_tfidf] # create a double wrapper over the original corpus: bow->tfidf->fold-in-lsi

                print "*********************** Attention les premiers topics *********************"
                print lsi.print_topics(2)
                Exemple['Latent Semantic Indexing'] = lsi.print_topics(2)
                #for doc in corpus_lsi: # both bow->tfidf and tfidf->lsi transformations are actually executed here, on the fly
                    #print(doc)


                ##############################S
                Candidats = []
                del laPlusPropre
                frequences=[]
                j=0
                while j<len(laPlusPropreAvecFreq):

                    if  (laPlusPropreAvecFreq[j+1] > 3) and (len(laPlusPropreAvecFreq[j])>3):
                        frequences.append(laPlusPropreAvecFreq[j+1])
                        Candidats.append(laPlusPropreAvecFreq[j])

                    j+=2

                s = [i[0] for i in sorted(enumerate(frequences), key=lambda x:x[1])]


                j=len(s)-1
                Autre=[]
                Last=[]
                while (j>=0):
                    pos = s[j]
                    Last.append(Candidats[pos])
                    Autre.append(Candidats[pos])
                    Last.append(frequences[pos])
                    j-=1


                for i in range(0, len(Autre)-1):
                    matchPlural= re.search(r's$',Autre[i])
                    if matchPlural:
                        single=Autre[i][:-1]
                        if single in Autre:
                            Autre[i]=single

                uniquelist = []

                [uniquelist.append(x) for x in Autre if x not in uniquelist]
                ##################Stanford POS Tagger AND NER
                print "Debut POS TAGGER ET NER STANFORD NLP"
                phrase = ""
                for mot in uniquelist:
                    phrase = phrase+' '+ mot
                Result = french_postagger.tag(phrase.split())
                temporaire = []
                for i in range (0, len(Result[0])-1):
                    if (Result[0][i][1]==u'NC') or (Result[0][i][1]==u'N')or (Result[0][i][1]==u'ADJ'):
                    ##if (Result[0][i][1]==u'VINF') or (Result[0][i][1]==u'ADV')or (Result[0][i][1]==u'VPP'):
                        #print Result[0][i][0]
                        #print Result[0][i][1]
                        temporaire.append(Result[0][i][0])
                Tab = []
                for mot in uniquelist:
                    if mot in temporaire:
                        Tab.append(mot)
                del uniquelist
                #uniquelist=[]
                uniquelist = Tab
                ############# Stanford NER ###########

                try:
                    organisationTemp = []
                    personTemp = []
                    locationTemp = []
                    col2 = hit["fields"]["file"][0]
                    NER=st.tag(col2.split())

                    for i in range(0, (len(NER[0])-1)):
                        if(NER[0][i][1] == u'ORGANIZATION'):
                            organisationTemp.append(NER[0][i][0])
                        if(NER[0][i][1] == u'LOCATION'):
                            locationTemp.append(NER[0][i][0])
                            print NER[0][i][0]
                        if(NER[0][i][1] == u'PERSON'):
                            personTemp.append(NER[0][i][0])

                except Exception, e:
                    print e
                organisation=[]
                location=[]
                person=[]
                [organisation.append(x) for x in organisationTemp if x not in organisation]
                [location.append(x) for x in locationTemp if x not in location]
                [person.append(x) for x in personTemp if x not in person]
                print "organisations:", organisation
                print "locations", location
                print "persons",person
                Exemple["Organisations_NER"]= organisation
                Exemple["Locations_NER"]= location
                Exemple["Noms_Personnes"]= person
                ##############
                j=0
                i=1
                SimilairePremier = []
                SimilaireDeuxieme =[]
                SimilaireTroisieme = []

                z = (len(uniquelist)-1)
                toSend=[]
                while j<z:
                    try:
                        premierMot= uniquelist[j]
                        while i<(len(uniquelist)-1):
                            try:

                                deuxiemeMot=uniquelist[i]
                                Taux= model.similarity(premierMot, deuxiemeMot)
                                if (Taux < 1.0000000000000000) and (Taux >= 0.5000000000000000):
                                    SimilairePremier.append(uniquelist[j])
                                    SimilaireDeuxieme.append(uniquelist[i])
                                    SimilaireTroisieme.append(Taux)
                                    toSend.append(uniquelist[j])
                                    toSend.append(uniquelist[i])
                                #print "result:",Last[j] , Last[i], Taux
                            except Exception, e:
                                #print e
                                pass
                            i+=1
                        j+=1
                        i = j+1
                        #print j
                    except Exception, e:
                        #print e
                        pass


                s = [i[0] for i in sorted(enumerate(SimilaireTroisieme), key=lambda x:x[1])]


                j=len(s)-1

                Ordered=[]
                while (j>=0):
                    pos = s[j]
                    Ordered.append(SimilairePremier[pos])
                    Ordered.append(SimilaireDeuxieme[pos])
                    Ordered.append(SimilaireTroisieme[pos])
                    j-=1

                #out_file = open("Ordred.json", "w")

                Exemple['similarité_word2vec'] = Ordered
                toTranslate=[]

                for i in range(0, len(toSend)-1):
                    matchPlural= re.search(r's$',toSend[i])
                    if matchPlural:
                        single=toSend[i][:-1]
                        if single in toSend:
                            #print single
                            #print toSend[i]
                            toSend[i]=single

                del uniquelist
                uniquelist = []

                [uniquelist.append(x) for x in toSend if x not in uniquelist]

                #print uniquelist

                print "nombre des candidats", len(uniquelist)

                finalList=[]
                for i in range (0, 20):
                    finalList.append(uniquelist[i])

                print finalList

                Exemple["candidats_Word2vec"] = finalList
                ################################# les liens #####################################
                try:
                    print '+++++++++++++++++++ liens ++++++++++++++++++++\n'
                    col2 = hit["fields"]["file"]
                    col2 = str(col2)
                    uri = ''
                    url = ''
                    total = 0
                    nbOK = 0
                    simpleList = []
                    for uri in extract_urls(col2):
                        uri = uri.replace('\\n', '')
                        uri = uri.replace('\\t', '')
                        uri = uri.replace('\\r', '')
                        uri = uri.replace('\\xad', '')
                        uri = uri.replace('\\u2010', '')
                        uri = uri.replace('nhttp', 'http')
                        if uri.startswith('www.'):
                           uri = "http://" + uri

                        if not uri.startswith('http'):
                                urii = "http://" + uri
                                ret = urllib2.urlopen(urii)
                                if ret.code == 200:
                                    uri = urii
                                else:
                                    uris = "https://" + uri
                                    ret = urllib2.urlopen(uris)
                                    if ret.code == 200:
                                        uri = uris
                                    else:
                                        uri = "http://" + uri

                        total += 1
                        try:
                            ret = urllib2.urlopen(uri)
                            r = requests.get(uri, allow_redirects=False)

                            if ret.code == 200:
                                print uri
                                if r.status_code == 301:
                                    stat = 'KO'
                                    print "redirection!"
                                else:
                                    print "Le site existe!"
                                    stat = 'OK'
                                    nbOK += 1
                                    try:
                                        f = urlopen(uri).read()
                                        tree = etree.HTML( f )
                                        meta=re.search( "<meta name=\"Keywords\".*?content=\"([^\"]*)\"", f ).group( 1 )
                                        #meta = tree.xpath( "//meta[@name='Keywords']" )[0].get("content")
                                        metaKeys.append(meta)
                                        #print  meta
                                    except Exception, e:
                                        #print e
                                        #meta = ""
                                        pass

                        except urllib2.HTTPError, e:
                            print uri
                            print(e.code)
                            stat = 'KO'
                        except urllib2.URLError, e:
                            print uri
                            print(e.args)
                            stat = 'KO'

                        lien = dict(url=uri, status=stat, lastchecked=now)
                        #x = lien
                        simpleList.append(lien)
                        #lien = {u"url": uri, u"status": stat, u"lastchecked": now }


                except Exception, e:
                    col2 = ""
                if total > 0:
                    percent = "{0:.0f}%".format(float(nbOK) / total * 100)
                else:
                    percent = "{0:.0f}%".format(float(0))
                Exemple['liens'] = simpleList
                Exemple['score'] = percent
                Exemple['WebSitesKeys'] = metaKeys

                ######################
                json.dump(Exemple, out_file, indent=4)
            except Exception, e:
                print e
                pass

        except Exception, e:
            print e
            pass


#print doc
print "*****************  Final part  *************************"
from sklearn.feature_extraction.text import TfidfVectorizer
tfidf_vectorizer = TfidfVectorizer()
tfidf_matrix = tfidf_vectorizer.fit_transform(doc)
print tfidf_matrix.shape


from sklearn.metrics.pairwise import cosine_similarity
print "*****************  COS Similarity entre les deux documents  *************************"
print documentName
print cosine_similarity(tfidf_matrix, tfidf_matrix)

####################  LSI CLUSTERING

from gensim import corpora, models, similarities
# remove common words and tokenize
#documents = doc
stoplist = set(StopWWords.split())
print stoplist
texts = [[word for word in document.lower().split() if word not in stoplist]
         for document in doc]


#print texts
#exit(0)
# remove words that appear only once
all_tokens = sum(texts, [])
tokens_once = set(word for word in set(all_tokens) if all_tokens.count(word) == 1)

texts = [[word for word in text if word not in tokens_once] for text in texts]

dictionary = corpora.Dictionary(texts)
corp = [dictionary.doc2bow(text) for text in texts]
#print corp
# extract 400 LSI topics; use the default one-pass algorithm
lsi = gensim.models.lsimodel.LsiModel(corpus=corp, id2word=dictionary, num_topics=400)
#print lsi
# print the most contributing words (both positively and negatively) for each of the first ten topics
print "*****************  lsiTopics  *************************"
print lsi.print_topics(20)


out_file.close()

